let adminUser = localStorage.getItem("isAdmin")
let storeData = window.location.search
console.log(storeData)

let params = new URLSearchParams(storeData);
console.log(params)

let courseId = params.get('courseId')
console.log(params.get('courseId'))

let token = localStorage.getItem("token")

fetch(`https://salty-plains-38523.herokuapp.com/api/courses/${courseId}`, {
	headers: {
		Authorization: `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	console.log(data)
	document.querySelector("#courseName").innerHTML = data.name
	document.querySelector("#courseDesc").innerHTML = data.description
	document.querySelector("#coursePrice").innerHTML = data.price
	// if(adminUser == 'true') {
	// 	fetch(`http://localhost:3000/api/users/`)
	// 	.then(res => res.json())
	// 	.then(data => {
	// 	document.querySelector("#enrollContainer").innerHTML = 
	// 	`
	// 	<h2> Enrollees: </h2>
	// 	<p> ${user.firstName}, ${user.lastName}</p>
	// 	`
	// }) else {

	document.querySelector("#enrollContainer").innerHTML = 
	`
	<button class="btn btn-block btn-success text-white enrollButton">
	Enroll
	</button>
	`
	})



document.querySelector("#enrollContainer").addEventListener("click", (e) => {
	e.preventDefault()

	fetch(`https://salty-plains-38523.herokuapp.com/api/users/enroll`, {
		method: "POST",
		headers: {
			Authorization: `Bearer ${token}`,
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			courseId: courseId
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
		if(data === true){
			alert("Enrolled Successfully!")
			window.location.replace("./courses.html")
		} else {
			alert("Something went wrong. Please try again!")
		}
		
	})
})
