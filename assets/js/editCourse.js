let storeData = window.location.search;
console.log(window.location.search);

let params = new URLSearchParams(window.location.search);

let courseId = params.get("courseId");
console.log(courseId);

let token = localStorage.getItem("token")

let courseEdit = document.querySelector("#editCourse");


fetch(`https://salty-plains-38523.herokuapp.com/api/courses/${courseId}`, {
	headers: {
		Authorization: `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	console.log(data)
	document.querySelector("#courseName").placeholder = data.name
	document.querySelector("#coursePrice").placeholder = data.price
	document.querySelector("#courseDescription").placeholder = data.description
})


courseEdit.addEventListener("submit", e => {
	e.preventDefault()

	let courseName = document.querySelector("#courseName").value;
	let coursePrice = document.querySelector("#coursePrice").value;
	let courseDescription = document.querySelector("#courseDescription").value;

	fetch(`https://salty-plains-38523.herokuapp.com/api/courses/${courseId}`, {
		method: "PUT",
		headers: {
			Authorization: `Bearer ${token}`,
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			"name": courseName,
			"price": coursePrice,
			"description": courseDescription
		})
	})
	.then(res => res.json())
	.then(data => {
		if(data === true) {
			alert("Course updated successfully!")
			window.location.replace("./courses.html")
		} else {
			alert("Something went wrong!")
		}
	})
})