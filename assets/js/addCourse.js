let courseAdd = document.querySelector("#createCourse");
console.log(courseAdd);

let token = localStorage.getItem("token")
console.log(token);

courseAdd.addEventListener("submit", e => {
	e.preventDefault()

	let courseName = document.querySelector("#courseName").value;
	let coursePrice = document.querySelector("#coursePrice").value;
	let courseDescription = document.querySelector("#courseDescription").value;

	fetch('https://salty-plains-38523.herokuapp.com/api/courses/', {
		method: "POST",
		headers: {
			Authorization: `Bearer ${token}`,
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			"name": courseName,
			"price": coursePrice,
			"description": courseDescription
		})
	})
	.then(res => res.json())
	.then(data => {
		if(data === true) {
			alert("You have created a new course!")
			window.location.replace("./courses.html")
		} else {
			alert("Something went wrong!")
		}
	})
})