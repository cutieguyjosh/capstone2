let loginForm = document.querySelector("#logInUser");
console.log(loginForm)

loginForm.addEventListener('submit', (e) => {
	e.preventDefault()

	let userEmail = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;

	if(userEmail === '' && password1 === '') {
		alert("Login failed! Missing input fields!")
	} else {
		fetch('https://salty-plains-38523.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: userEmail,
				password: password
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data.accessToken) {
				//successful authentication will return JSON web token
				localStorage.setItem('token', data.accessToken);

				fetch('https://salty-plains-38523.herokuapp.com/api/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					//set the global user state to have properties containing authenticated user's ID and role
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					window.location.replace("./courses.html")
				})
			} else {
				alert("Authentication failure")
			}
		})
	}

})