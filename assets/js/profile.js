// let token = localStorage.getItem('token')
// let userId = localStorage.getItem('id')
// let profile = document.querySelector("#profile")
// let tables = document.querySelector("#tables")

// console.log(profile)
// console.log(userId)
// fetch('http://localhost:3000/api/users/details', {
// 	method: "GET",
// 	headers: {
// 		Authorization: `Bearer ${token}`,
// 		"Content-Type": "application/json"
// 	}
// })
// .then(res => res.json())
// .then(data => {
// 	profile.innerHTML = 
// 	`
// 	<h2>First Name: ${data.firstName} </h2>
// 	<h2>Last Name: ${data.lastName} </h2>
// 	<h2>Email: ${data.email} </h2>
// 	<h2>Mobile No.: ${data.mobileNo} </h2>
// 	<table class="table" id="enrollments">
// 		<thead>
// 			<tr>
// 				<th scope="col">Course</th>
// 				<th scope="col">Enrolled On</th>
// 				<th scope="col">Status</th>
// 			</tr>
// 		</thead>
// 		<tbody id="tableBody">
// 		</tbody>
// 		</table>
// 	`

// let tables = document.querySelector("#tableBody")
// 	userData = data.enrollments.map(result => {
// 		return(
// 			`
// 			<tr>
// 				<th scope="row">${result.courseId}</th>
// 				<td>${result.enrolledOn}</td>
// 				<td>${result.status}</td>
// 			</tr>
// 			`
// 			)
// 	}).join('')
// tables.innerHTML = userData
// })

let token = localStorage.getItem('token');
let userProfile = document.querySelector('#profileContainer');

if (!token) alert('Something went wrong')
else {
    fetch('https://salty-plains-38523.herokuapp.com/api/users/details', {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
    .then(res => res.json())
    .then(userData => {
        if (!userData) return false;
        else {
            userProfile.innerHTML = 
                `
                <div class="col-md-12">
                    <div class="">
                        <p class="text-center h5 font-weight-bold">
                            First Name: ${userData.firstName}
                        </p>
                        <p class="text-center h5 font-weight-bold">
                            Last Name: ${userData.lastName}
                        </p>
                        <p class="text-center h5 font-weight-bold">
                            Email: ${userData.email}
                        </p>
                        <p class="text-center h5 font-weight-bold">
                            Mobile Number: ${userData.mobileNo}
                        </p>
                        <p class="h3 font-weight-bold">
                            Class History
                        </p>
                        <table class="w-100 my-5">
                            <thead>
                                <tr>
                                    <th>Course Name</th>
                                    <th>Course ID</th>
                                    <th class="text-center">Enrolled On</th>
                                    <th class="text-right">Status</th>
                                </tr>
                            </thead>
                            <tbody id="tBody">
                                <tr class="d-flex"></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                `
            let table = document.querySelector('#tBody');
            let row
            userData.enrollments.map(userInfo => {
                if (!userInfo) alert('No such data found!')
                fetch('https://salty-plains-38523.herokuapp.com/api/courses')
                .then(result => result.json())
                .then(courseData => {
                    if (courseData.length < 1) alert('There is no such course')
                    row = courseData.map(course => {
                        if(course._id === userInfo.courseId) {
                            return table.innerHTML += (
                                `
                                <td class="my-5 text-left">${course.name}</td>
                                <td class="my-5 text-left">${course._id}</td>
                                <td class="my-5 text-center">${userInfo.enrolledOn}</td>
                                <td class="my-5 text-right">${userInfo.status}</td>
                                `
                            ) + '<br/>'
                        }
                    }).join('')
                    // table.innerHTML = row
                    console.log(table)
                })
            })
            
        }
    })
}