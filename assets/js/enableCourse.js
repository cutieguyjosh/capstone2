let storeData = window.location.search
console.log(window.location.search)

let params = new URLSearchParams(window.location.search)

let courseId = params.get("courseId")
console.log(courseId)

let token = localStorage.getItem("token")

fetch(`https://salty-plains-38523.herokuapp.com/api/courses/enableCourse/${courseId}`, {
	method: "PUT",
	headers: {
		Authorization: `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	if(data === true) {
		window.location.replace("./courses.html");
	} else {
		alert("Something went wrong!")
	}
})