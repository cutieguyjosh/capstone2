
let registerForm = document.querySelector("#registerUser");
console.log(registerForm);



registerForm.addEventListener('submit', (e) => {
	e.preventDefault()

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let userEmail = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;
	let registerUser = document.querySelector("#registerUser").value;

 	//Validation to enable submit button when all fields are populated and both passwords match
 	//Mobile number is = 11

	if((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNumber.length === 11)) {
		//check database
		//check for duplicate email in database first
		//url - where we can get the duplicate routes in our server
		//asynchronous load contents of the URL
		fetch('https://salty-plains-38523.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				"email": userEmail
			})
		})
		//return a promise that resolves when result is loaded
		.then(res => res.json()) //call this function when result is loaded
		.then(data => {
			if(data === false){

			//if no duplicates found
			//get the routes for registration in our server
			fetch('https://salty-plains-38523.herokuapp.com/api/users/', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					"firstName": firstName,
					"lastName": lastName,
					"email": userEmail,
					"password": password1,
					"mobileNo": mobileNumber
				})
			})
			.then(res => {
				return res.json() //same as res => res.json()
			})
			.then(data => {
				console.log(data)
				//if registration is successful
				if(data === true) {
					alert("Registered successfully")
					//redirect to login page
					window.location.replace("./login.html")
				} 
				// else {
				// 	//error occured in registration
				// }

			})

		} else {
			alert("Email already exist.")
		}	

		})
	} else {
		alert("Something went wrong, please try again!")
	}


})











// function onSubmit(e) {
// 	e.preventDefault()
// 	console.log(`FirstName: ${firstName.value}`)
// 	console.log(`Last Name: ${lastName.value}`)
// 	console.log(`Mobile Number: ${mobileNumber.value}`)
// 	console.log(`Email: ${userEmail.value}`)
// 	console.log(`Password: ${password1.value}`)
// 	console.log(`Verifired password: ${password2.value}`)

// 	//Validation to enable submit button when all fields are populated and both passwords match
// 	//Mobile number is = 11
// 	if(firstName.value === "" || lastName.value === "" || mobileNumber.value === "" || userEmail.value === "" || password1.value === "" || password2.value === "") {
// 		alert("Registration failed! Please complete all details!");
// 	} else if(mobileNumber.value.length !== 11) {
// 		alert("Registration failed! Mobile number is less than 11 digits!")
// 	}else if(password1.value != password2.value) {
// 		alert("Registration failed! Password does not match!");
// 	} else {
// 		alert("Registration success! User is now registered!")
// 	}

// 	firstName.value = ""
// 	lastName.value = ""
// 	mobileNumber.value = ""
// 	userEmail.value = ""
// 	password1.value = ""
// 	password2.value = ""
// }



